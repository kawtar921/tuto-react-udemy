import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import colorScreen from './src/screens/colorScreen';
import CountScreen from './src/screens/CountScreen';
import HomeScreen from "./src/screens/HomeScreen";
import ImageScreen from './src/screens/ImageScreen';
import ListScreen from './src/screens/ListScreen';
import LoginComponent from "./src/screens/LoginComponent"

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Login : LoginComponent,
    List : ListScreen,
    ImageScreen : ImageScreen,
    Counter : CountScreen,
    Color : colorScreen
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      title: "App"
    }
  }
);

export default createAppContainer(navigator);
