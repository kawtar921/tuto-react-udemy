import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ImageDetail from './components/ImageDetail';

export default ImageScreen = () => {
    return (
        <View>
             <ImageDetail title = "Forest" imageSrc={require('../../assets/forest.jpg')}/>
             <ImageDetail title = "Beach" imageSrc={require('../../assets/beach.jpg')}/>
             <ImageDetail title = "Mountain" imageSrc={require('../../assets/mountain.jpg')}/>
        </View>
       
    );
};

const style = StyleSheet.create({

});