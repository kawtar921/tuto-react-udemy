import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

const LoginComponent = () => {
    return (
    <View>
        <Text style={style.font}>Login component works</Text>
    </View>
    )
    
};

const style = StyleSheet.create({
    font : {
        fontSize : 30
    }
});

export default LoginComponent;