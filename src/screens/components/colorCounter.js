import React, { useState } from 'react';
import { View, StyleSheet, Button, FlatList } from 'react-native';

export default colorCounter = ({color, onDecrease, onIncrease}) => {
    return (
        <View>
            <Text>{color}</Text>
            <Button title = {`Increase ${color}`} onPress = {onIncrease()}/>
            <Button title = {`Decrease ${color}`} onPress = {onDecrease()}/>
        </View>
    )
}