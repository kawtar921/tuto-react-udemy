import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const HomeScreen = ({navigation}) => {

  return (
    <View>
      <Text style={styles.text}>HomeScreen</Text>
      <Button 
        onPress = {() => navigation.navigate("Login")}
        title = "Go to login Demo"
         />

      <Button
        onPress = {() => navigattion.navigate('List')}
        title = "Go to List Component"
      />

      <Button
        onPress = {() => navigattion.navigate('ImageScreen')}
        title = "Go to Image screen Component"
      />

      <Button
        onPress = {() => navigattion.navigate('Counter')}
        title = "Go to Image counter Component"
      />

      <Button
        onPress = {() => navigattion.navigate('Color')}
        title = "Go to color Component"
      />

    </View>

  );
 
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
