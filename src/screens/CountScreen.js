import React, { useState } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

export default CountScreen = () => {

    let [counter,setCounter] = useState(0)

    return <View>
        <Button title = "Increase" onPress={() => { setCounter(count + 1);}}/>
        <Button title = "Decrease" onPress={() => { setCounter(count - 1);}}/>
        <Text>Current Count : {counter} </Text>
    </View>
}

const styles = StyleSheet.create({

});