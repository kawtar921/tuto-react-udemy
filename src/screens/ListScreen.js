import React from 'react';
import { Text, StyleSheet, FlatList } from 'react-native';

export default ListScreen = () => {

    const friends = [
        { name : 'Friend #1' , age : 20 },
        { name : 'Friend #2' , age : 30 },
        { name : 'Friend #3' , age : 40 },
        { name : 'Friend #4' , age : 50 },
        { name : 'Friend #5' , age : 60 },
        { name : 'Friend #6' , age : 70 },
        { name : 'Friend #7' , age : 80 }
        
    ];

    return (
        <FlatList k
            keyExtractor = { friend => friend.name }
            data = { friends }
            renderItem = { ({item}) => {
                return (
                    <Text>
                        {item.name} - Age = {item.age}
                    </Text>
                );
            }}
        >

        </FlatList>
    );
}